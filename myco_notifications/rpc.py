import pika
import uuid
import json


class AbstractQueue(object):

    def __init__(self, queue='', exchange='', routing_key='', server_name='rabbit', port=5672, username='guest', password='guest', host='/'):
        self.queue = queue
        self.exchange = exchange
        self.routing_key = routing_key
        self.server_name = server_name
        self.port = port
        self.username = username
        self.password = password
        self.host = host
        self.credentials = pika.PlainCredentials(self.username, self.password)
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(self.server_name, self.port, self.host, self.credentials))
        self.channel = self.connection.channel()
        self.set_queue()

    def render_content(self, method='', params=''):
        return {'jsonrpc': '2.0', 'method': method, 'params': params}

    def set_queue(self):
        raise NotImplementedError("Not implemented")

    def call(self):
        raise NotImplementedError("Not implemented")


class SyncCall(AbstractQueue):
    """
        Method used for external calls where
        you need to wait for a response to follow the flow.
        The call below is blocking, so the code flow
        interrupt in the call loop until you receive
        response of the requested resource.

        For this case we pass beyond the message and the 
        sending queue another queue that will be responsible
        for receiving the response from the external resource.
    """

    def set_queue(self):
        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue
        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            body = body.decode("utf-8")
            body = json.loads(body)
            self.response = body

    def call(self, method, content):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange=self.exchange,
                                   routing_key=self.queue,
                                   properties=pika.BasicProperties(
                                       reply_to=self.callback_queue,
                                       correlation_id=self.corr_id,
                                   ),
                                   body=json.dumps(self.render_content(method, content)))
        while self.response is None:
            self.connection.process_data_events()

        return self.response


class AsyncCall(AbstractQueue):

    """
        In this method we have a non-blocking behavior,
        so the process will follow its flow soon after
        confirming that RabbitMQ has added the task in
        the queue. 

        We set up properties for the creation
        of the task as the delivery_mode property, which
        ensures that the task will be resilient and 
        executed even if RabbitMQ is dropped or the 
        services performing the task are not currently
        available. 

        See more: https://www.rabbitmq.com/tutorials/tutorial-two-python.html
    """

    def set_queue(self):
        self.channel.queue_declare(queue=self.queue, durable=True)

    def call(self, method, content):
        self.channel.basic_publish(exchange=self.exchange,
                                   routing_key=self.queue,
                                   body=json.dumps(
                                       self.render_content(method, content)),
                                   properties=pika.BasicProperties(
                                       delivery_mode=2,  # make message persistent
                                   ))
        self.connection.close()


class EventBroadcasted(AbstractQueue):

    """
        The method below follows the RabbitMQ topic 
        convention to distribute a broadcast message 
        to an exchange that will be responsible for 
        delivering the messages to the workers who 
        are listening to a particular route pattern. 
        The definition of routes and syntax can be 
        found in the official documentation below.  

        See more: https://www.rabbitmq.com/tutorials/tutorial-five-python.html
    """

    def set_queue(self):
        self.channel.exchange_declare(exchange=self.exchange,
                                      exchange_type='topic')

    def call(self, method, content):
        message = json.dumps(self.render_content(
            method, content), default=str, sort_keys=True)
        self.channel.basic_publish(exchange=self.exchange,
                                   routing_key=self.routing_key,
                                   body=message)
