import json
from .rpc import AsyncCall, SyncCall


class NotificationsInterface(object):

    def __init__(self, server_name='rabbit', port=5672, username='guest', password='guest', host='/'):
        self.server_name = server_name
        self.port = port
        self.username = username
        self.password = password
        self.host = host
        self.sync_channel = SyncCall(
            queue='sync_notifications',
            server_name=self.server_name,
            port=self.port,
            username=self.username,
            password=self.password,
            host=self.host
        )

    """
        We need open a assync channel before each request, because 
        this will closed after that a message is sended.
    """

    def open_assync_call_channel(self):
        self.assync_channel = AsyncCall(
            queue='async_notifications',
            server_name=self.server_name,
            port=self.port,
            username=self.username,
            password=self.password,
            host=self.host
        )

    """
        Create a notification to an user with some basics informations.
        Target_type represents a subject about the notification: ex.: 'project_invitation"
        Target_id is a local primary key of target_type linked with a notification.
        The possible values to action are: C(create),  E(edit), D(delete) and U(update).
    """

    def create_notification(self, user_id, target_type, data, text, action, created=None, readed=None):
        actions_values = {'C', 'E', 'D', 'U'}
        if action not in actions_values:
            raise ('The action value is not a valid choice!')

        body = {
            "user_id": str(user_id),
            "target_type": str(target_type),
            "data": data,
            "text": str(text),
            "action": str(action)
        }

        if created:
            body['created'] = str(created)

        if readed:
            body['readed'] = str(readed)

        self.open_assync_call_channel()
        self.assync_channel.call('create_notifications', body)

    """
        Used for set a notification as read. 
        The notification will be only returned in list_notifications with readed=True.
    """

    def read_notification(self, notification_id, user_id):

        body = {'notification_id': str(
            notification_id), 'user_id': str(user_id)}

        self.open_assync_call_channel()
        self.assync_channel.call('read_notification', body)

    """
        In this case we have a physical delete of a notification.
    """

    def delete_notification(self, notification_id, user_id):

        body = {'notification_id': str(
            notification_id), 'user_id': str(user_id)}

        self.open_assync_call_channel()
        self.assync_channel.call('delete_notification', body)

    """
        This functions return a list with notifications where the filter has been match. 
        Unlike other functions, here we are using a SyncCall on RPC funtions 
        so we will waiting until get a response.
    """

    def list_notifications(self, user_id, readed=None, target_type=None, offset=None):

        body = {
            'user_id': str(user_id),
        }
        if readed:
            body['readed'] = str(readed)

        if target_type:
            body['target_type'] = str(target_type)

        if offset:
            body['offset'] = str(offset)

        return self.sync_channel.call('list_notifications', body)
