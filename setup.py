# -*- coding: utf-8 -*-

from setuptools import setup


setup(
    name='myco_notifications',
    version='0.0.1',
    author='MyCo',
    author_email='tassio.noronha@devngo.fr',
    include_package_data=True,
    url='https://bitbucket.org/mycoteam/myco-notifications-interface/',
    license='MIT',
    description='A librariy to interact with the myco notifications service',
    long_description='A librariy to interact with the myco notifications service',
    keywords='myco notifications rabbitmq',
    zip_safe=False,
    install_requires=[
        'requests>2.0',
        'pika>0.12',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
